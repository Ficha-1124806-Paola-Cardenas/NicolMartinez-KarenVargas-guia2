-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-05-2016 a las 03:32:46
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `hospital2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE IF NOT EXISTS `cita` (
  `IdCita` bigint(20) NOT NULL AUTO_INCREMENT,
  `FechaCita` date DEFAULT NULL,
  `HoraCita` time NOT NULL,
  `LugarCita` varchar(100) DEFAULT NULL,
  `Consultorio` bigint(20) NOT NULL,
  `FkIdTipoCita` bigint(20) NOT NULL,
  `FkIdPaciente` bigint(20) NOT NULL,
  `FkIdDoctor` bigint(20) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdCita`),
  KEY `citapaciente` (`FkIdPaciente`),
  KEY `citadoctor` (`FkIdDoctor`),
  KEY `citatipo` (`FkIdTipoCita`),
  KEY `citaestado` (`FkEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`IdCita`, `FechaCita`, `HoraCita`, `LugarCita`, `Consultorio`, `FkIdTipoCita`, `FkIdPaciente`, `FkIdDoctor`, `FkEstado`) VALUES
(1, '2015-05-04', '04:30:45', '20 Julio', 405, 1, 1, 1, 1),
(2, '2015-05-04', '05:40:47', 'kennedy', 101, 2, 2, 2, 1),
(3, '2016-06-05', '03:10:20', '20 Julio', 204, 10, 11, 10, 1),
(4, '2016-08-15', '05:30:54', 'Kennedy', 303, 3, 3, 3, 1),
(5, '2016-04-02', '02:30:41', 'Chapinero', 502, 2, 4, 4, 1),
(6, '2016-03-08', '01:30:41', 'Restrepo', 102, 5, 5, 5, 1),
(7, '2016-04-08', '07:05:41', 'Chapinero', 602, 6, 6, 6, 1),
(8, '2016-07-21', '08:04:51', 'Chapinero', 401, 7, 7, 7, 1),
(9, '2016-06-12', '02:50:41', '20 Julio', 102, 8, 8, 8, 1),
(10, '2016-05-14', '06:05:12', 'Restrepo', 503, 9, 9, 9, 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `citasactivas`
--
CREATE TABLE IF NOT EXISTS `citasactivas` (
`Codigo` bigint(20)
,`Fecha` date
,`Hora` time
,`Lugar` varchar(100)
,`Consultorio` bigint(20)
,`TipoCita` bigint(20)
,`Paciente` varchar(50)
,`Doctor` varchar(50)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `citasinactivas`
--
CREATE TABLE IF NOT EXISTS `citasinactivas` (
`Codigo` bigint(20)
,`Fecha` date
,`Hora` time
,`Consultorio` bigint(20)
,`Doctor` varchar(100)
,`Paciente` varchar(100)
,`Area` varchar(100)
,`Lugar` varchar(100)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotamoderadora`
--

CREATE TABLE IF NOT EXISTS `cuotamoderadora` (
  `IdCuota` bigint(20) NOT NULL AUTO_INCREMENT,
  `Regimen` varchar(100) NOT NULL,
  `TotalPagar` bigint(20) NOT NULL,
  `FkIdOperario` bigint(20) NOT NULL,
  `FkIdPaciente` bigint(20) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdCuota`),
  KEY `cuotaope` (`FkIdOperario`),
  KEY `cuotapaci` (`FkIdPaciente`),
  KEY `cuotaesta` (`FkEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `cuotamoderadora`
--

INSERT INTO `cuotamoderadora` (`IdCuota`, `Regimen`, `TotalPagar`, `FkIdOperario`, `FkIdPaciente`, `FkEstado`) VALUES
(1, 'Subsidiado', 10, 1, 1, 1),
(2, 'Contributivo', 5, 2, 2, 1),
(3, 'Contributivo', 3, 3, 3, 1),
(4, 'Subsidiado', 6, 4, 4, 1),
(5, 'Contributivo', 12, 1, 4, 1),
(6, 'Subsidiado', 2, 4, 5, 1),
(7, 'Contributivo', 3, 5, 5, 1),
(8, 'Subsidiado', 2, 6, 6, 1),
(9, 'Subsidiado', 1, 7, 7, 1),
(10, 'Contributivo', 5, 3, 7, 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `cuotasnopagas`
--
CREATE TABLE IF NOT EXISTS `cuotasnopagas` (
`Codigo` bigint(20)
,`Total` bigint(20)
,`Paciente` varchar(100)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `cuotaspagas`
--
CREATE TABLE IF NOT EXISTS `cuotaspagas` (
`Codigo` bigint(20)
,`Total` bigint(20)
,`Paciente` varchar(100)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallefaccompra`
--

CREATE TABLE IF NOT EXISTS `detallefaccompra` (
  `IdDetalleFC` bigint(20) NOT NULL AUTO_INCREMENT,
  `Cantidad` bigint(20) NOT NULL,
  `SubTotal` bigint(20) NOT NULL,
  `Iva` bigint(20) NOT NULL,
  `FkIdMedicamento` bigint(20) NOT NULL,
  `FkIdFacCompra` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDetalleFC`),
  KEY `facdeta` (`FkIdFacCompra`),
  KEY `detamedi` (`FkIdMedicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico`
--

CREATE TABLE IF NOT EXISTS `diagnostico` (
  `IdDiagnostico` bigint(20) NOT NULL AUTO_INCREMENT,
  `Fecha` date DEFAULT NULL,
  `Sintomas` varchar(200) NOT NULL,
  `Examenes` varchar(200) DEFAULT NULL,
  `Descripcion` varchar(200) NOT NULL,
  `Valoracion` varchar(200) NOT NULL,
  `FkIdPaciente` bigint(20) NOT NULL,
  `FkIdDoctor` bigint(20) NOT NULL,
  `FkIdMedicamento` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDiagnostico`),
  KEY `doctordiag` (`FkIdDoctor`),
  KEY `diagpaci` (`FkIdPaciente`),
  KEY `diagmedi` (`FkIdMedicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
  `IdDoctor` bigint(20) NOT NULL AUTO_INCREMENT,
  `FkIdTipoD` bigint(20) NOT NULL,
  `NumeroDocumento` bigint(20) NOT NULL,
  `NombreD` varchar(50) NOT NULL,
  `ApellidoD` varchar(50) NOT NULL,
  `TelefonoD` bigint(20) NOT NULL,
  `EMailD` varchar(50) NOT NULL,
  `Cargo` varchar(200) NOT NULL,
  `Especialidad` varchar(200) NOT NULL,
  `Experiencia` varchar(200) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `FkIdEps` bigint(20) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDoctor`),
  KEY `tipodoc` (`FkIdTipoD`),
  KEY `epsdoc` (`FkIdEps`),
  KEY `doctortesta` (`FkEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `doctor`
--

INSERT INTO `doctor` (`IdDoctor`, `FkIdTipoD`, `NumeroDocumento`, `NombreD`, `ApellidoD`, `TelefonoD`, `EMailD`, `Cargo`, `Especialidad`, `Experiencia`, `Direccion`, `FkIdEps`, `FkEstado`) VALUES
(1, 2, 52060517, 'Pedro', 'Lamus', 3625695, 'Pedro@hotmail.com', 'Jefe', 'Pediatra', '5 años', 'cll34N34-45', 1, 1),
(2, 2, 794825498, 'Hamilton', 'Montero', 6598598, 'Hamiltonn@gmail.com', 'Auxiliar', 'Odontologia', '2 años', 'cll45N23-43', 7, 1),
(3, 2, 1023588987, 'Paola', 'Casas', 321585841, 'Paola@gmail.com', 'Jefe', 'Doctora medicina general', '3 años', 'cll 34n-23-45', 8, 1),
(4, 2, 51256456, 'Camila', 'Fagua', 985254415, 'CamilaFa@hotmail.com', 'Jefe', 'Psiquiatra', '5 años', 'crr 45 N 56-65', 9, 1),
(5, 2, 98561452, 'Yeison', 'Londoño', 3625689, 'Yeison@gmail.com', 'Jefe', '', 'Opotometria', 'cll34N56-67', 10, 1),
(6, 2, 526051041, 'Ricardo', 'Ochoa', 3625265, 'Ricardo@hotmail.com', 'Auxiliar', 'Traumatologia', '2 años', 'cll45N32-23', 6, 1),
(7, 2, 548956254, 'Janneth', 'Perez', 3625645, 'Janneth@hotmail.com', 'Jefe', 'Oncologia', '8 años', 'aven4512-23', 5, 1),
(8, 2, 98565488, 'Jonathan', 'Linares', 5624512, 'Jonathan@gmail.com', 'Jefe', 'Ginecologia', '4 añoS', 'aven45N56-34', 4, 1),
(9, 2, 52468512, 'Maicol', 'Guzman', 52654252, 'Maicol@hotmail.com', 'Auxiliar', 'Optometria', '1 año', 'avenida 43 N 34-63', 3, 1),
(10, 2, 6548154544, 'Lorena', 'Roa', 562584141, 'LoreRoa@gamil.com', 'Auxiliar', 'Pedriatria', '2 años', 'cll 45 N 21-32', 10, 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `doctoractivo`
--
CREATE TABLE IF NOT EXISTS `doctoractivo` (
`Codigo` bigint(20)
,`Documento` bigint(20)
,`Nombre` varchar(100)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `doctorinactivo`
--
CREATE TABLE IF NOT EXISTS `doctorinactivo` (
`Codigo` bigint(20)
,`Documento` bigint(20)
,`Nombre` varchar(100)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eps`
--

CREATE TABLE IF NOT EXISTS `eps` (
  `IdEps` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreE` varchar(50) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdEps`),
  KEY `epstesta` (`FkEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `eps`
--

INSERT INTO `eps` (`IdEps`, `NombreE`, `FkEstado`) VALUES
(1, 'Compensar', 1),
(2, 'Cafesalud', 1),
(3, 'Sura', 1),
(4, 'Sanitas', 1),
(5, 'Nueva Eps', 1),
(6, 'Capital Salud', 1),
(7, 'Caprecom', 1),
(8, 'Coomeva', 1),
(9, 'Caprecom', 1),
(10, 'Coomeva', 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `epsactiva`
--
CREATE TABLE IF NOT EXISTS `epsactiva` (
`Codigo` bigint(20)
,`Nombre` varchar(50)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `epsinactiva`
--
CREATE TABLE IF NOT EXISTS `epsinactiva` (
`Codigo` bigint(20)
,`Nombre` varchar(50)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `IdEstado` bigint(20) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`IdEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`IdEstado`, `Nombre`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `facturaactiva`
--
CREATE TABLE IF NOT EXISTS `facturaactiva` (
`Codigo` bigint(20)
,`FormaPago` varchar(100)
,`Fecha` date
,`Proveedor` varchar(100)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacompra`
--

CREATE TABLE IF NOT EXISTS `facturacompra` (
  `IdFacturaCom` bigint(20) NOT NULL AUTO_INCREMENT,
  `FormaPago` varchar(100) NOT NULL,
  `FechaCompra` date DEFAULT NULL,
  `Descripcion` varchar(200) NOT NULL,
  `ValorTotal` bigint(20) NOT NULL,
  `FkIdProv` bigint(20) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdFacturaCom`),
  KEY `profac` (`FkIdProv`),
  KEY `facttesta` (`FkEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `facturanopaga`
--
CREATE TABLE IF NOT EXISTS `facturanopaga` (
`Codigo` bigint(20)
,`FormaPago` varchar(100)
,`Fecha` date
,`Proveedor` varchar(100)
,`estado` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialinactivo`
--

CREATE TABLE IF NOT EXISTS `historialinactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Paciente` varchar(100) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialmedico`
--

CREATE TABLE IF NOT EXISTS `historialmedico` (
  `IdHistorial` bigint(20) NOT NULL,
  `FechaModificacion` date NOT NULL,
  `FkIdPaciente` bigint(20) NOT NULL,
  `FkIdDiagnostico` bigint(20) NOT NULL,
  `FkIdCita` bigint(20) NOT NULL,
  PRIMARY KEY (`IdHistorial`),
  KEY `pacientehisto` (`FkIdPaciente`),
  KEY `diahisto` (`FkIdDiagnostico`),
  KEY `histocita` (`FkIdCita`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventarioactivo`
--

CREATE TABLE IF NOT EXISTS `inventarioactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Inventario` varchar(100) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventarioinactivo`
--

CREATE TABLE IF NOT EXISTS `inventarioinactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Inventario` varchar(100) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventariomedi`
--

CREATE TABLE IF NOT EXISTS `inventariomedi` (
  `IdInventario` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreInv` varchar(100) NOT NULL,
  `FechaCreacion` date DEFAULT NULL,
  `StockMaximo` bigint(20) NOT NULL,
  `StockMinimo` bigint(20) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdInventario`),
  KEY `Invenesta` (`FkEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Volcado de datos para la tabla `inventariomedi`
--

INSERT INTO `inventariomedi` (`IdInventario`, `NombreInv`, `FechaCreacion`, `StockMaximo`, `StockMinimo`, `Descripcion`, `FkEstado`) VALUES
(31, 'Analgesicos', '2012-05-04', 1000, 200, 'Reduce dolores fuertes', 2),
(32, 'Jarabes', '2015-05-21', 1000, 300, ' Son las preparaciones líquidas, con alta concentración de carbohidratos', 1),
(33, 'Ampolla de inyeccion', '2015-01-06', 1200, 200, 'disolución ,se aplica mediante una jeringa ', 1),
(34, 'Gotas', '2016-05-12', 1200, 200, 'Medicamento o sustancia que se toma o se emplea dosificada en gotas.', 1),
(35, 'ungüentos', '2012-05-06', 1200, 200, 'Crema  preparacion semisólida para el tratamiento tópico', 1),
(36, 'sueros', '2013-04-21', 1300, 200, 'solución formada por diversos compuestos del organismo ( suero fisiológico)', 1),
(37, 'Antisepticos', '2016-06-25', 1500, 200, 'sustancias químicas que se aplican sobre la piel y las mucosas', 1),
(38, 'Antibioticos', '2016-03-04', 1200, 200, 'medicamentos potentes que combaten las infecciones bacterianas.', 1),
(39, 'Laxantes', '2016-07-28', 1600, 200, 'es una preparación usada para provocar la defecación o la eliminación de heces', 1),
(40, 'Antidepresivos', '2016-03-15', 1500, 200, 'medicinas que receta un médico para tratar la depresión', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamento`
--

CREATE TABLE IF NOT EXISTS `medicamento` (
  `IdMedicamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreMedi` varchar(100) NOT NULL,
  `Lote` bigint(20) NOT NULL,
  `FechaVencimiento` date DEFAULT NULL,
  `Descripcion` varchar(100) DEFAULT NULL,
  `Contraindicaciones` varchar(100) NOT NULL,
  `FormaFarmaceutica` varchar(100) NOT NULL,
  `Concentracion` varchar(100) NOT NULL,
  `FkIdInventario` bigint(20) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdMedicamento`),
  KEY `mediinv` (`FkIdInventario`),
  KEY `mediesta` (`FkEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamentosinvencer`
--

CREATE TABLE IF NOT EXISTS `medicamentosinvencer` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `FechaDeVencimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamentovencido`
--

CREATE TABLE IF NOT EXISTS `medicamentovencido` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `FechaDeVencimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operario`
--

CREATE TABLE IF NOT EXISTS `operario` (
  `IdOperario` bigint(20) NOT NULL AUTO_INCREMENT,
  `FkIdTipoD` bigint(20) NOT NULL,
  `NumeroDocumento` bigint(20) NOT NULL,
  `NombreO` varchar(50) NOT NULL,
  `ApellidoO` varchar(50) NOT NULL,
  `TelefonoO` bigint(20) NOT NULL,
  `EMailO` varchar(50) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `FkIdEps` bigint(20) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdOperario`),
  KEY `epsoperario` (`FkIdEps`),
  KEY `tipodoperario` (`FkIdTipoD`),
  KEY `operaesta` (`FkEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `operario`
--

INSERT INTO `operario` (`IdOperario`, `FkIdTipoD`, `NumeroDocumento`, `NombreO`, `ApellidoO`, `TelefonoO`, `EMailO`, `Direccion`, `FkIdEps`, `FkEstado`) VALUES
(1, 2, 52064875, 'Jose', 'Murrilo', 362545, 'Jose@hotmail.com', 'cll39an23-30', 6, 1),
(2, 2, 1245681254, 'Laura', 'Lopez', 3625484, 'Luisa@gmail.com', 'avenida 43a N 34-54', 9, 1),
(3, 2, 6524842, 'Juan', 'Camargo', 3624857, 'Juanchito@gamil.com', 'crr34b N34-54', 7, 1),
(4, 2, 524869512, 'Paula', 'Gonzalez', 325415, 'Pulita@gmail.com', 'avenida45cN34-45', 8, 1),
(5, 2, 6559856, 'Felipe ', 'Moreno', 6254821, 'Camilo@gmail.com', 'Cll34bN34-45', 5, 1),
(6, 2, 52648950, 'Cristian', 'Becerra', 36254511, 'Cristin@gmail.com', 'Crr32cN34-45', 4, 1),
(7, 2, 36524854, 'Maria', 'Alvarez', 3625487, 'Mari@gmail.com', 'Cll45dN34-45', 5, 1),
(8, 2, 3562485, 'Jairo', 'Bentacur', 5268495, 'Jairo@hotmail.com', 'avenid45bN-45-43', 2, 1),
(9, 2, 79548526, 'Alberto', 'Dominguez', 2725292, 'ALbwerto@gmail.com', 'cll34dN45-21', 7, 1),
(10, 2, 74562845, 'Brayan', 'Gomez', 5680940, 'Brayitan', 'cll34dN34-25', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operarioactivo`
--

CREATE TABLE IF NOT EXISTS `operarioactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operarioinactivo`
--

CREATE TABLE IF NOT EXISTS `operarioinactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
  `IdPaciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `FkIdTipoD` bigint(20) NOT NULL,
  `NumeroDoc` bigint(20) NOT NULL,
  `NombreP` varchar(50) NOT NULL,
  `ApellidoP` varchar(50) NOT NULL,
  `TelefonoP` bigint(20) NOT NULL,
  `EMailP` varchar(50) NOT NULL,
  `CelularP` bigint(20) NOT NULL,
  `EdadP` bigint(20) NOT NULL,
  `GeneroP` varchar(10) NOT NULL,
  `Ocupacion` varchar(50) NOT NULL,
  `FkIdEps` bigint(20) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdPaciente`),
  KEY `tipodocpaci` (`FkIdTipoD`),
  KEY `epspaci` (`FkIdEps`),
  KEY `paciesta` (`FkEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`IdPaciente`, `FkIdTipoD`, `NumeroDoc`, `NombreP`, `ApellidoP`, `TelefonoP`, `EMailP`, `CelularP`, `EdadP`, `GeneroP`, `Ocupacion`, `FkIdEps`, `Direccion`, `FkEstado`) VALUES
(1, 3, 68415684, 'Luisa', 'Martin', 52454, 'luisa@gmail.com', 541584, 40, 'Femenino', 'Desempleado', 3, 'cra 6 sur', 1),
(2, 4, 25458, 'Hector', 'Lopez', 524568, 'hector@gmail.com', 524528, 30, 'Masculino', 'Empleado', 3, 'cra 9 este', 1),
(3, 4, 151358212, 'Luis', 'Perez', 865845, 'luisito@gmail.com', 55854755, 36, 'Masculino', 'Empleado', 5, 'cra 4 este', 1),
(4, 3, 2584524, 'Juan', 'Lopez', 36854285, 'juan@gmail.com', 254254, 45, 'Masculino', 'Empleado', 2, 'cra 6', 1),
(5, 1, 982657845, 'Julian', 'Lopez', 3521442, 'juli@hotmail.com', 31358468, 18, 'masculino', 'Estudiante', 1, 'cll34#2r-34', 1),
(6, 2, 8743639432765, 'Jose', 'Montaña', 355264, 'josemon@gmail.com', 65521544, 25, 'masculino', 'Empleado', 3, 'cll4ºN23-45', 1),
(7, 2, 854584785, 'Liliana', 'Ariza', 3215648, 'dlili@hotmail', 3256584, 41, 'Femenino', 'Empleado', 2, 'cr45N34-45', 1),
(8, 3, 52651656, 'Martina', 'Hernandez', 685225, 'Martina_2@gmail.com', 3521545, 25, 'Femenino', 'Empleado', 3, 'cll34N34-56', 1),
(9, 3, 98562455, 'Juliana', 'Montero', 3625645, 'Juli@gmail.com', 354584, 26, 'Femenino', 'Empleado', 2, 'cll34N34-24', 1),
(11, 1, 596845454, 'Juliana', 'Rodriguez', 65235245, 'JulianaRodri@gmail.com', 36252144, 22, 'Femenino', 'Docente', 1, 'cll45N34-45', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacienteactivo`
--

CREATE TABLE IF NOT EXISTS `pacienteactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacienteinactivo`
--

CREATE TABLE IF NOT EXISTS `pacienteinactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `IdProveedor` bigint(20) NOT NULL AUTO_INCREMENT,
  `FkIdTipoD` bigint(20) NOT NULL,
  `NumeroDoc` bigint(20) NOT NULL,
  `NombrePro` varchar(100) NOT NULL,
  `DireccionPro` varchar(100) NOT NULL,
  `TelefonoPro` bigint(20) NOT NULL,
  `EMailPro` varchar(100) NOT NULL,
  `FkEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdProveedor`),
  KEY `tipopro` (`FkIdTipoD`),
  KEY `provedeesta` (`FkEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedoractivo`
--

CREATE TABLE IF NOT EXISTS `proveedoractivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedorinactivo`
--

CREATE TABLE IF NOT EXISTS `proveedorinactivo` (
  `Codigo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocita`
--

CREATE TABLE IF NOT EXISTS `tipocita` (
  `IdTipoCita` bigint(20) NOT NULL AUTO_INCREMENT,
  `AreaCita` varchar(100) NOT NULL,
  PRIMARY KEY (`IdTipoCita`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `tipocita`
--

INSERT INTO `tipocita` (`IdTipoCita`, `AreaCita`) VALUES
(1, 'Pediatria'),
(2, 'Medicina General'),
(3, 'Odontologia'),
(4, 'Ginecologia'),
(5, 'Optometria'),
(6, 'Cardiologia'),
(7, 'Oncologia'),
(8, 'Psiquiatria'),
(9, 'Traumatologia'),
(10, 'Preferencial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodocumento`
--

CREATE TABLE IF NOT EXISTS `tipodocumento` (
  `IdTipoD` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(10) NOT NULL,
  PRIMARY KEY (`IdTipoD`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `tipodocumento`
--

INSERT INTO `tipodocumento` (`IdTipoD`, `Nombre`) VALUES
(1, 'TI'),
(2, 'CC'),
(3, 'CE'),
(4, 'RC'),
(5, 'DNI'),
(6, 'DUI'),
(7, 'NIT');

-- --------------------------------------------------------

--
-- Estructura para la vista `citasactivas`
--
DROP TABLE IF EXISTS `citasactivas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `citasactivas` AS select `cita`.`IdCita` AS `Codigo`,`cita`.`FechaCita` AS `Fecha`,`cita`.`HoraCita` AS `Hora`,`cita`.`LugarCita` AS `Lugar`,`cita`.`Consultorio` AS `Consultorio`,`cita`.`FkIdTipoCita` AS `TipoCita`,`paciente`.`NombreP` AS `Paciente`,`doctor`.`NombreD` AS `Doctor`,`estado`.`Nombre` AS `estado` from (((`cita` join `paciente`) join `doctor`) join `estado`) where ((`cita`.`FkEstado` = 1) and (`estado`.`Nombre` = 'Activo'));

-- --------------------------------------------------------

--
-- Estructura para la vista `citasinactivas`
--
DROP TABLE IF EXISTS `citasinactivas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `citasinactivas` AS select `cita`.`IdCita` AS `Codigo`,`cita`.`FechaCita` AS `Fecha`,`cita`.`HoraCita` AS `Hora`,`cita`.`Consultorio` AS `Consultorio`,concat(`doctor`.`NombreD`,`doctor`.`ApellidoD`) AS `Doctor`,concat(`paciente`.`NombreP`,`paciente`.`ApellidoP`) AS `Paciente`,`tipocita`.`AreaCita` AS `Area`,`cita`.`LugarCita` AS `Lugar`,`estado`.`Nombre` AS `estado` from ((((`cita` join `tipocita`) join `estado`) join `doctor`) join `paciente`) where ((`cita`.`FkEstado` = 1) and (`estado`.`Nombre` = 'Activo'));

-- --------------------------------------------------------

--
-- Estructura para la vista `cuotasnopagas`
--
DROP TABLE IF EXISTS `cuotasnopagas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cuotasnopagas` AS select `cuotamoderadora`.`IdCuota` AS `Codigo`,`cuotamoderadora`.`TotalPagar` AS `Total`,concat(`paciente`.`NombreP`,`paciente`.`ApellidoP`) AS `Paciente`,`estado`.`Nombre` AS `estado` from ((`cuotamoderadora` join `estado`) join `paciente`) where ((`cuotamoderadora`.`FkEstado` = 4) and (`estado`.`Nombre` = 'No Pago'));

-- --------------------------------------------------------

--
-- Estructura para la vista `cuotaspagas`
--
DROP TABLE IF EXISTS `cuotaspagas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cuotaspagas` AS select `cuotamoderadora`.`IdCuota` AS `Codigo`,`cuotamoderadora`.`TotalPagar` AS `Total`,concat(`paciente`.`NombreP`,`paciente`.`ApellidoP`) AS `Paciente`,`estado`.`Nombre` AS `estado` from ((`cuotamoderadora` join `estado`) join `paciente`) where ((`cuotamoderadora`.`FkEstado` = 3) and (`estado`.`Nombre` = 'Pago'));

-- --------------------------------------------------------

--
-- Estructura para la vista `doctoractivo`
--
DROP TABLE IF EXISTS `doctoractivo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `doctoractivo` AS select `doctor`.`IdDoctor` AS `Codigo`,`doctor`.`NumeroDocumento` AS `Documento`,concat(`doctor`.`NombreD`,`doctor`.`ApellidoD`) AS `Nombre`,`estado`.`Nombre` AS `estado` from (`doctor` join `estado`) where ((`doctor`.`FkEstado` = 1) and (`estado`.`Nombre` = 'Activo'));

-- --------------------------------------------------------

--
-- Estructura para la vista `doctorinactivo`
--
DROP TABLE IF EXISTS `doctorinactivo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `doctorinactivo` AS select `doctor`.`IdDoctor` AS `Codigo`,`doctor`.`NumeroDocumento` AS `Documento`,concat(`doctor`.`NombreD`,`doctor`.`ApellidoD`) AS `Nombre`,`estado`.`Nombre` AS `estado` from (`doctor` join `estado`) where ((`doctor`.`FkEstado` = 2) and (`estado`.`Nombre` = 'Inactivo'));

-- --------------------------------------------------------

--
-- Estructura para la vista `epsactiva`
--
DROP TABLE IF EXISTS `epsactiva`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `epsactiva` AS select `eps`.`IdEps` AS `Codigo`,`eps`.`NombreE` AS `Nombre`,`estado`.`Nombre` AS `estado` from (`eps` join `estado`) where ((`eps`.`FkEstado` = 1) and (`estado`.`Nombre` = 'Activo'));

-- --------------------------------------------------------

--
-- Estructura para la vista `epsinactiva`
--
DROP TABLE IF EXISTS `epsinactiva`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `epsinactiva` AS select `eps`.`IdEps` AS `Codigo`,`eps`.`NombreE` AS `Nombre`,`estado`.`Nombre` AS `estado` from (`eps` join `estado`) where ((`eps`.`FkEstado` = 2) and (`estado`.`Nombre` = 'Inactivo'));

-- --------------------------------------------------------

--
-- Estructura para la vista `facturaactiva`
--
DROP TABLE IF EXISTS `facturaactiva`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `facturaactiva` AS select `facturacompra`.`IdFacturaCom` AS `Codigo`,`facturacompra`.`FormaPago` AS `FormaPago`,`facturacompra`.`FechaCompra` AS `Fecha`,`proveedor`.`NombrePro` AS `Proveedor`,`estado`.`Nombre` AS `estado` from ((`facturacompra` join `proveedor`) join `estado`) where ((`facturacompra`.`FkEstado` = 3) and (`estado`.`Nombre` = 'Pago'));

-- --------------------------------------------------------

--
-- Estructura para la vista `facturanopaga`
--
DROP TABLE IF EXISTS `facturanopaga`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `facturanopaga` AS select `facturacompra`.`IdFacturaCom` AS `Codigo`,`facturacompra`.`FormaPago` AS `FormaPago`,`facturacompra`.`FechaCompra` AS `Fecha`,`proveedor`.`NombrePro` AS `Proveedor`,`estado`.`Nombre` AS `estado` from ((`facturacompra` join `proveedor`) join `estado`) where ((`facturacompra`.`FkEstado` = 4) and (`estado`.`Nombre` = 'No Pago'));

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `cita_ibfk_4` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`FkIdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `cita_ibfk_2` FOREIGN KEY (`FkIdDoctor`) REFERENCES `doctor` (`IdDoctor`),
  ADD CONSTRAINT `cita_ibfk_3` FOREIGN KEY (`FkIdTipoCita`) REFERENCES `tipocita` (`IdTipoCita`);

--
-- Filtros para la tabla `cuotamoderadora`
--
ALTER TABLE `cuotamoderadora`
  ADD CONSTRAINT `cuotamoderadora_ibfk_3` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `cuotamoderadora_ibfk_1` FOREIGN KEY (`FkIdOperario`) REFERENCES `operario` (`IdOperario`),
  ADD CONSTRAINT `cuotamoderadora_ibfk_2` FOREIGN KEY (`FkIdPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Filtros para la tabla `detallefaccompra`
--
ALTER TABLE `detallefaccompra`
  ADD CONSTRAINT `detallefaccompra_ibfk_1` FOREIGN KEY (`FkIdFacCompra`) REFERENCES `facturacompra` (`IdFacturaCom`),
  ADD CONSTRAINT `detallefaccompra_ibfk_2` FOREIGN KEY (`FkIdMedicamento`) REFERENCES `medicamento` (`IdMedicamento`);

--
-- Filtros para la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD CONSTRAINT `diagnostico_ibfk_1` FOREIGN KEY (`FkIdDoctor`) REFERENCES `doctor` (`IdDoctor`),
  ADD CONSTRAINT `diagnostico_ibfk_2` FOREIGN KEY (`FkIdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `diagnostico_ibfk_3` FOREIGN KEY (`FkIdMedicamento`) REFERENCES `medicamento` (`IdMedicamento`);

--
-- Filtros para la tabla `doctor`
--
ALTER TABLE `doctor`
  ADD CONSTRAINT `doctor_ibfk_3` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`FkIdTipoD`) REFERENCES `tipodocumento` (`IdTipoD`),
  ADD CONSTRAINT `doctor_ibfk_2` FOREIGN KEY (`FkIdEps`) REFERENCES `eps` (`IdEps`);

--
-- Filtros para la tabla `eps`
--
ALTER TABLE `eps`
  ADD CONSTRAINT `eps_ibfk_1` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`);

--
-- Filtros para la tabla `facturacompra`
--
ALTER TABLE `facturacompra`
  ADD CONSTRAINT `facturacompra_ibfk_2` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `facturacompra_ibfk_1` FOREIGN KEY (`FkIdProv`) REFERENCES `proveedor` (`IdProveedor`);

--
-- Filtros para la tabla `historialmedico`
--
ALTER TABLE `historialmedico`
  ADD CONSTRAINT `historialmedico_ibfk_1` FOREIGN KEY (`FkIdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `historialmedico_ibfk_2` FOREIGN KEY (`FkIdDiagnostico`) REFERENCES `diagnostico` (`IdDiagnostico`),
  ADD CONSTRAINT `historialmedico_ibfk_3` FOREIGN KEY (`FkIdCita`) REFERENCES `cita` (`IdCita`);

--
-- Filtros para la tabla `inventariomedi`
--
ALTER TABLE `inventariomedi`
  ADD CONSTRAINT `inventariomedi_ibfk_1` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`);

--
-- Filtros para la tabla `medicamento`
--
ALTER TABLE `medicamento`
  ADD CONSTRAINT `medicamento_ibfk_2` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `medicamento_ibfk_1` FOREIGN KEY (`FkIdInventario`) REFERENCES `inventariomedi` (`IdInventario`);

--
-- Filtros para la tabla `operario`
--
ALTER TABLE `operario`
  ADD CONSTRAINT `operario_ibfk_3` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `operario_ibfk_1` FOREIGN KEY (`FkIdEps`) REFERENCES `eps` (`IdEps`),
  ADD CONSTRAINT `operario_ibfk_2` FOREIGN KEY (`FkIdTipoD`) REFERENCES `tipodocumento` (`IdTipoD`);

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `paciente_ibfk_3` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `paciente_ibfk_1` FOREIGN KEY (`FkIdTipoD`) REFERENCES `tipodocumento` (`IdTipoD`),
  ADD CONSTRAINT `paciente_ibfk_2` FOREIGN KEY (`FkIdEps`) REFERENCES `eps` (`IdEps`);

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`FkIdTipoD`) REFERENCES `tipodocumento` (`IdTipoD`),
  ADD CONSTRAINT `proveedor_ibfk_2` FOREIGN KEY (`FkEstado`) REFERENCES `estado` (`IdEstado`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
